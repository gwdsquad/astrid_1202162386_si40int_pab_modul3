package com.astrid.userpc.modul3;

public class Akun {

    private String Nama;
    private String Pekerjaan ;
    private String JenisKelamin ;
    private int Thumbnail ;

    public Akun() {
    }

    public Akun(String nama, String pekerjaan, String jenisKelamin, int thumbnail) {
        Nama = nama;
        Pekerjaan = pekerjaan;
        JenisKelamin = jenisKelamin;
        Thumbnail = thumbnail;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getPekerjaan() {
        return Pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        Pekerjaan = pekerjaan;
    }

    public String getJenisKelamin() {
        return JenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        JenisKelamin = jenisKelamin;
    }

    public int getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        Thumbnail = thumbnail;
    }




}
