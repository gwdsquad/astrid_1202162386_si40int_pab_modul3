package com.astrid.userpc.modul3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {


    private TextView tvnama,tvpekerjaan,tvkelamin;
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_);

        tvnama = (TextView) findViewById(R.id.textnama);
        tvpekerjaan = (TextView) findViewById(R.id.textpekerjaan);
        tvkelamin = (TextView) findViewById(R.id.textkelamin);
        img = (ImageView) findViewById(R.id.akunthumbnail);

        // Recieve data
        Intent intent = getIntent();
        String Nama = intent.getExtras().getString("Nama");
        String Pekerjaan = intent.getExtras().getString("Pekerjaan");
        String Kelamin = intent.getExtras().getString("Kelamin");
        int image = intent.getExtras().getInt("Thumbnail") ;

        // Setting values
        tvnama.setText(Nama);
        tvpekerjaan.setText(Pekerjaan);
        tvkelamin.setText(Kelamin);
        img.setImageResource(image);


    }
}
